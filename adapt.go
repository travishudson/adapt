package adapt

import (
    "os"
    "fmt"
    "sort"
    "sync"
    "math"
    "time"
    "math/rand"
    "encoding/gob"
)

//
// Implement this interface to use adapt.Solve(), which will atempt to solve the problem using a genetic algorithm
//
type GAProblem interface {
    MeasureFitness(genome []byte) int
    AllValidBytes() []byte
    GenomeLength() int
}

//
// A collection of individuals
//
type Population struct {
    Individuals []Individual
    GenerationCount int       // number of generations completed by this population
    HighestFitness int        // highest fitness in the population
    FitnessStagnation int     // number of most recent generations in which the highest fitness has not improved
}

//
// A member of the population, representing a possible solution
//
type Individual struct {
    Genome []byte
    Fitness int          // fitness rating, measured by heuristic function (starts at -1)
    AncestorCount int    // how many predecessor individuals led to this one, starting at 0
}

//
// All the configuration needed to run a genetic algorithm
//
//     firstPopulations -- list of Populations to start with; set to nil to have new populations generated from scratch
//     maxGenerations   -- stop after this many generations have been made (default: 300)
//     maxDuration      -- stop after this amount of time has passed (default: 10 minutes)
//     populationSize   -- number of individuals in each population (default: 300)
//     toKill           -- how many members of the population to kill each generation (default: 150)
//     immuneCount      -- how many of the most fit are immune from being killed each generation (default: 5)
//     transferRate     -- how many generations to wait in fitness stagnation before transfering some Individuals between Populations (default: 5)
//     mutationPolicy   -- specifies how mutation probability changes over time
//     goroutineCount   -- the number of goroutines to use
//
type GAConfig struct {
    Problem GAProblem
    FirstPopulations []Population
    MaxGenerations int
    MaxDuration time.Duration
    PopulationSize int
    ToKill int
    ImmuneCount int
    TransferRate int
    MutationPolicy MutationPolicy
    GoroutineCount int
}

//
// The probability out of 1,000 that a byte in a Gene will mutate is related to fitnessStagnation.
// The equation is: m = y(fitnessStagnation) + z
// y and z are all arbitrarily chosen floating point numbers between -3 and 3, inclusive.
//
type MutationPolicy struct {
    Y float32 // Y and Z are used in a linear equation that makes mutation probability correlate with generation count
    Z float32
}

//
// Print summary information about a list of Populations
//
func SummarizePopulations(populations []Population) {
    fmt.Printf("\n%40s\n", "=== Summary of Populations ===")
    fmt.Printf("%-42s %d\n", "Number of populations:", len(populations))

    var fitnesses []int
    for _,pop := range populations {
        fitnesses = append(fitnesses, pop.Individuals[0].Fitness)
    }
    sort.Sort(sort.Reverse(sort.IntSlice(fitnesses)))
    fmt.Printf("%-42s %v\n", "Fitness, high to low:", fitnesses)

    ancestorCount := -1
    for _,pop := range populations {
        for _,individual := range pop.Individuals {
            if individual.Fitness == fitnesses[0] {
                ancestorCount = individual.AncestorCount
            }
        }
    }
    fmt.Printf("%-42s %d\n", "Ancestor count of the most fit individual:", ancestorCount)
    fmt.Printf("%-42s %d\n", "Generation count of the first population:", populations[0].GenerationCount)
    fmt.Printf("%-42s %d\n", "Ending stagnation of the first population:", populations[0].FitnessStagnation)
    fmt.Printf("%-42s %.2f%%\n", "Efficiency:", (float64(ancestorCount)/float64(populations[0].GenerationCount)) * 100)
    fmt.Println()
}

//
// Print a configuration in a human-readable format, except for Problem and FirstPopulations
//
func PrintConfig(config GAConfig) {
    fmt.Printf("populationSize: %d\n", config.PopulationSize)
    fmt.Printf("toKill: %d\n", config.ToKill)
    fmt.Printf("immuneCount: %d\n", config.ImmuneCount)
    fmt.Printf("transferRate: %d\n", config.TransferRate)
    fmt.Printf("mutationPolicy Y,Z: %f, %f\n", config.MutationPolicy.Y, config.MutationPolicy.Z)

    // These values are not modified by examples/meta, so they are shown below the fold
    fmt.Println()
    fmt.Printf("maxGenerations: %d\n", config.MaxGenerations)
    fmt.Printf("maxDuration: %v\n", config.MaxDuration)
    fmt.Printf("goroutineCount: %d\n", config.GoroutineCount)
}

//
// Return the most fit Individual among all populations
//
func FindMostFitIndividual(populations []Population) Individual {
    var bestIndividual Individual
    bestFitness := math.MinInt32
    for _,pop := range populations {
        for _,individual := range pop.Individuals {
            if individual.Fitness > bestFitness {
                bestFitness = individual.Fitness
                bestIndividual = individual
            }
        }
    }
    return bestIndividual
}

//
// Save a list of populations to persistent storage, ready to be read and analyzed later
//
func SerializePopulations(populations []Population, fileName string) {

    // Create gob file to save to
    file, err := os.Create(fileName)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    } else {

        // Write serialized []Population to file
        defer file.Close()
        encoder := gob.NewEncoder(file)
        encoder.Encode(populations)
    }
}

//
// Read a list of populations from a saved gob file
//
func UnserializePopulations(fileName string) []Population {

    // Open a gob file for reading
    file, err := os.Open(fileName)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    } else {

        // Read file to []Population and return it
        defer file.Close()
        var output []Population
        decoder := gob.NewDecoder(file)
        err := decoder.Decode(&output)

        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }

        return output
    }

    return []Population{}  // return an empty slice if an error occurred
}

//
// Make a starting population of individuals from scratch
//
func MakePopulation(problem GAProblem, size int, goroutineCount int) Population {
    var population Population
    population.Individuals = makeIndividuals(problem, nil, size, -1, goroutineCount)
    population.GenerationCount = 0  // number of generations completed by this population
    population.FitnessStagnation = 0 // number of most recent generations in which the highest fitness has not improved
    population.HighestFitness = math.MinInt32        // highest fitness in the population
    return population
}

//
// Generate one offspring from two parent individuals
//
func generateOffspring(problem GAProblem, parent1 Individual, parent2 Individual, mutationRate int, generator *rand.Rand) Individual {

    if len(parent1.Genome) != len(parent2.Genome) {
        panic("Error: Not equal lengths")
    }

    var result []byte
    if generator.Intn(100) > 2 {
        for i:=0; i<len(parent1.Genome); i++ {
            if flipCoin(generator) {
                result = append(result, parent1.Genome[i])
            } else {
                result = append(result, parent2.Genome[i])
            }
        }
    } else {
        if flipCoin(generator) {
            result = append(result, parent1.Genome...)
        } else {
            result = append(result, parent2.Genome...)
        }
    }

    result = mutate(problem, result, mutationRate, generator)
    return Individual{Fitness: problem.MeasureFitness(result), Genome: result, AncestorCount: incrementAncestorCount(parent1, parent2)}
}

//
// Return true or false with a 50/50 chance
//
func flipCoin(generator *rand.Rand) bool {
    return generator.Intn(2) == 0
}

//
// Return the greater ancestor count between two parent individuals plus one
//
func incrementAncestorCount(parent1 Individual, parent2 Individual) int {
    if parent1.AncestorCount > parent2.AncestorCount {
        return parent1.AncestorCount + 1
    } else {
        return parent2.AncestorCount + 1
    }
}

//
// Return a copy of a genome that may be mutated
//
func mutate(problem GAProblem, genome []byte, mutationRate int, generator *rand.Rand) []byte {
    var result []byte
    for i:=0; i<len(genome); i++ {
        if generator.Intn(1000) < mutationRate {
            result = append(result, problem.AllValidBytes()[generator.Intn(len(problem.AllValidBytes()))])  // pick a random new byte
        } else {
            result = append(result, genome[i])     // keep the existing byte, unmutated
        }
    }
    return result
}

//
// Return a whole number that specifies how many times out of 1,000 any given byte in a Genome will mutate
// The expression is: y(fitnessStagnation) + z
// fitnessStagnation is the number of generations that have passed with no increased fitness for the most fit individual in the population.
// y and z are all arbitrarily chosen numbers between -3 and 3, inclusive.
//
func calculateMutationRate(policy MutationPolicy, fitnessStagnation int) int {
    return int(policy.Y * float32(fitnessStagnation) + policy.Z)
}

//
// Find the maximum fitness value among the given individuals
//
func findHighestFitness(population Population) int {
    maxFitness := math.MinInt32
    for i:=0; i<len(population.Individuals); i++ {
        if population.Individuals[i].Fitness > maxFitness {
            maxFitness = population.Individuals[i].Fitness
        }
    }
    return maxFitness
}

//
// Find the minimum fitness value among the given individuals
//
func findMinFitness(population Population) int {
    minFitness := population.Individuals[0].Fitness
    for i:=1; i<len(population.Individuals); i++ {
        if population.Individuals[i].Fitness < minFitness {
            minFitness = population.Individuals[i].Fitness
        }
    }
    return minFitness
}

//
// Remove individual at the specified index
//
func deleteFromPop(population *Population, index int) {
    population.Individuals = append(population.Individuals[:index], population.Individuals[index+1:]...)
}

//
// Delete some of the less fit members
// population must already be sorted by fitness with most fit at the beginning
// k is the number to kill
// immune specifies the number of most fit members of the population that won't be killed (2 means the two most fit are immune)
//
func kill(population *Population, k int, immune int, generator *rand.Rand) {
    for i:=0; i<k; i++ {

        // find the highest extisting fitness value of all individuals
        maxFitness := findHighestFitness(*population)

        // build a list of integers that track how far off the corresponding individual was from the max fitness
        var shortOfMax []int
        totalShort := 0       // the total difference from max for all individuals
        for n:=0; n<len(population.Individuals); n++ {
            short := maxFitness - population.Individuals[n].Fitness
            totalShort += short
            shortOfMax = append(shortOfMax, short)
        }

        // if the total is 0, then kill anything; there's no reason to choose one over another
        if totalShort == 0 {
            deleteFromPop(population, generator.Intn(len(population.Individuals) - immune) + immune)
        } else {
            inc := 0
            r := generator.Intn(totalShort)
            for n:=immune; n<len(population.Individuals); n++ {
                if inc + shortOfMax[n] > r {
                    deleteFromPop(population, n)
                    break
                }
                inc += shortOfMax[n]
            }
        }

    }
}

//
// Return the index for a random member of the population, with fitness increasing the likelihood of selection
//
func weightedSelect(population Population, generator *rand.Rand) int {

    // calculate a number to add to all fitness values that makes them positive
    adjustment := 0
    minFitness := findMinFitness(population)
    if minFitness < 0 {
        adjustment = -minFitness + 1  // +1 because totalFitness can't end up being 0
    }

    // find the total fitness among all members
    totalFitness := 0
    for i:=0; i<len(population.Individuals); i++ {
        totalFitness += population.Individuals[i].Fitness + adjustment
    }

    inc := 0
    r := generator.Intn(totalFitness)
    for i:=0; i<len(population.Individuals); i++ {
        if inc + population.Individuals[i].Fitness + adjustment > r {
            return i
        }
        inc += population.Individuals[i].Fitness + adjustment
    }

    // should not get this far
    panic("Unexpected event in weightedSelect()")
    return -1
}

//
// Return two unique indexes, each representing a weighted random member of the population
//
func selectTwo(population Population, generator *rand.Rand) (int, int) {
    if len(population.Individuals) < 2 {
        panic("Error: Cannot select 2 from a population size less than 2")
    }

    if len(population.Individuals) == 2 {
        return 0, 1
    }

    index1 := weightedSelect(population, generator)
    index2 := weightedSelect(population, generator)

    for index1 == index2 {
        index2 = weightedSelect(population, generator)
    }

    return index1, index2
}

//
// Use goroutineCount number of goroutines to make toMake number of Individuals
// If population is not nil, then new Individuals will be based on existing Individuals in that population
// mutationRate is ignored if population is nil
//
func makeIndividuals(problem GAProblem, population *Population, toMake int, mutationRate int, goroutineCount int) []Individual {

    // Ensure a reasonable goroutineCount
    if goroutineCount < 1 { goroutineCount = 1 }

    // Build up a list of new Individuals to return as the result
    var newIndividuals []Individual
    for i:=0; i<toMake; i++ {
        newIndividuals = append(newIndividuals, Individual{})
    }

    // Populate newIndividuals using goroutines running in parallel
    var wg sync.WaitGroup
    assignments := assignGoroutines(toMake, goroutineCount)
    for i:=0; i<goroutineCount; i++ {
        wg.Add(1)
        go func(indexesToChange []int) {
            defer wg.Done()
            generator := rand.New(rand.NewSource(time.Now().UTC().UnixNano()))
            for _,index := range(indexesToChange) {
                if population == nil {
                    var newGenome []byte
                    for n:=0; n<problem.GenomeLength(); n++ {
                        newGenome = append(newGenome, problem.AllValidBytes()[generator.Intn(len(problem.AllValidBytes()))])
                    }

                    newIndividuals[index] = Individual{Fitness: problem.MeasureFitness(newGenome), Genome: newGenome, AncestorCount: 0}
                } else {
                    parent1, parent2 := selectTwo(*population, generator)
                    newIndividuals[index] = generateOffspring(problem, population.Individuals[parent1], population.Individuals[parent2], mutationRate, generator)
                }
            }
        }(assignments[i])
    }
    wg.Wait()

    return newIndividuals
}

//
// The opposite of kill -- uses generateOffspring() to add new members to the population
// toAdd is the number of new members to add
//
func repopulate(problem GAProblem, population *Population, toAdd int, mutationRate int, goroutineCount int) {
    newIndividuals := makeIndividuals(problem, population, toAdd, mutationRate, goroutineCount)
    population.Individuals = append(population.Individuals, newIndividuals...)
}

//
// TODO: Returning a two dimensional slice is confusing. Return a struct instead?
// Make a list, with each item being a list of slots (usually indexes of Individuals) that a particular goroutine should be assigned to work on. Each slot can be assigned to only one goroutine.
// This is used to spread out the work that multiple goroutines are doing in parallel to create new Individuals.
//
func assignGoroutines(slotsRemaining int, goroutineCount int) [][]int {

    // Make a list as long as the number of goroutines to store the results
    assignments := [][]int{}
    for i:=0; i<goroutineCount; i++ {
        assignments = append(assignments, []int{})
    }

    // Solve this problem recursively
    return assignGoroutines_(slotsRemaining, assignments)
}

func assignGoroutines_(slotsRemaining int, assignments [][]int) [][]int {

    // No goroutines to assign slots to
    if len(assignments) == 0 {
        return assignments
    }

    // No slots to assign goroutines to
    if slotsRemaining <= 0 {
        return assignments
    }

    // Decide what slots the next goroutine should be assigned to
    slotCount := slotsRemaining / len(assignments)
    for i:=0; i<slotCount; i++ {
        slotsRemaining--
        assignments[0] = append(assignments[0], slotsRemaining)
    }
    return append([][]int{assignments[0]}, assignGoroutines_(slotsRemaining, assignments[1:])...)
}

//
// Produce an error if any configuration settings are not valid, return nil otherwise
//
func validateGAConfig(config GAConfig) error {

    if len(config.Problem.AllValidBytes()) == 0 {
        return fmt.Errorf("AllValidBytes() cannot have length 0.")
    }
    if config.Problem.GenomeLength() <= 0 {
        return fmt.Errorf("GenomeLength() must return a value greater than 0. The value returned was %d.", config.Problem.GenomeLength())
    }
    if len(config.FirstPopulations) < 0 {
        return fmt.Errorf("FirstPopulations cannot have negative length.")
    }
    if config.MaxGenerations <= 0 {
        return fmt.Errorf("MaxGenerations must be greater than 0. The value given was %d.", config.MaxGenerations)
    }
    if config.MaxDuration <= 0 {
        return fmt.Errorf("MaxDuration must be greater than 0. The value given was %v.", config.MaxDuration)
    }
    if config.PopulationSize <= 0 {
        return fmt.Errorf("PopulationSize must be greater than 0. The value given was %d.", config.PopulationSize)
    }
    if config.ToKill <= 0 {
        return fmt.Errorf("ToKill must be greater than 0. The value given was %d.", config.ToKill)
    }
    if config.ImmuneCount < 0 {
        return fmt.Errorf("ImmuneCount must be 0 or greater. The value given was %d.", config.ImmuneCount)
    }
    if config.TransferRate <= 0 {
        return fmt.Errorf("TransferRate must be greater than 0. The value given was %d.", config.TransferRate)
    }
    if config.GoroutineCount <= 0 {
        return fmt.Errorf("GoroutineCount must be greater than 0. The value given was %d.", config.GoroutineCount)
    }
    if config.ToKill > config.PopulationSize {
        return fmt.Errorf("ToKill must not be greater than PopulationSize. ToKill is %d; PopulationSize is %d.", config.ToKill, config.PopulationSize)
    }
    if config.ImmuneCount > config.PopulationSize {
        return fmt.Errorf("ImmuneCount must not be greater than PopulationSize. ImmuneCount is %d; PopulationSize is %d.", config.ImmuneCount, config.PopulationSize)
    }
    if config.ToKill + config.ImmuneCount > config.PopulationSize {
        return fmt.Errorf("ToKill + ImmuneCount must not be greater than PopulationSize. ToKill is %d; ImmuneCount is %d; PopulationSize is %d.", config.ToKill, config.ImmuneCount, config.PopulationSize)
    }

    return nil
}

