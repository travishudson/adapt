#!/bin/sh

#
# Run this Bash script to start a new tmux session called "adapt" and attach to it.
# This sets up a nice development environment, where there's a tmux window open to the Desktop for building,
# alongside other windows that can be used for doing git stuff or vim stuff. Modify this script to suit your development preferences.
#
# The command "chmod 755 start-tmux-session.sh" might be necessary so that running this file is possible.
#

# Set up the "git" window for version control activities
tmux new-session -d -c "$GOPATH/src/bitbucket.org/travishudson/adapt" -s adapt
tmux rename-window "git"

# Set up the "build" window for building code
tmux new-window -c "$HOME/Desktop"
tmux rename-window "build"

# Set up a "code" window for source code editing activities
tmux new-window -c "$GOPATH/src/bitbucket.org/travishudson/adapt"
tmux rename-window "code"

# Select the first window created by this script, then attach to the session
tmux select-window -t git
tmux attach-session -d -t adapt

