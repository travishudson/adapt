package nqueens

//
// The nqueens package is for solving the n queens problem, where n queens must be placed on a chess board without attacking each other.
//

import (
    "strings"
)

const BOARD_SIZE = 32           // board width and height
const BOARD_SPACES = BOARD_SIZE * BOARD_SIZE

type NQueensProblem struct {}

func (prob NQueensProblem) MeasureFitness(genome []byte) int {
    return rateBoard(string(genome))
}

func (prob NQueensProblem) AllValidBytes() []byte {
    return []byte("Qx")
}

func (prob NQueensProblem) GenomeLength() int {
    return BOARD_SPACES    // every gene in the genome represents one space on the board
}

//
// Return an integer from 0 to 1000 that measures how fit a board possition is, with higher numbers being more fit
//
func rateBoard(board string) int {
    rating := 0

    // examine all columns
    for x:=0; x<BOARD_SIZE; x++ {

        // check all spaces in this column for queens
        queenCount := 0
        for i:=x; i<BOARD_SPACES; i+=BOARD_SIZE {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // examine all rows
    for y:=0; y<BOARD_SIZE; y++ {

        // check all spaces in this row for queens
        queenCount := 0
        for i:=y*BOARD_SIZE; i<(y*BOARD_SIZE)+BOARD_SIZE; i++ {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // examine all forward-slanted diagonals on the top half of the board
    //      checks these array indexes on an 8x8 board (each row is one iteration):
    //      0
    //      8,  1
    //      16, 9,  2
    //      24, 17, 10, 3
    //      ...
    for y:=0; y<BOARD_SPACES; y+=BOARD_SIZE {

        // check all spaces on this diagonal for queens
        queenCount := 0
        for i:=y; i>=0; i-=BOARD_SIZE-1 {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // examine all forward-slanted diagonals on the bottom half of the board
    //      checks these array indexes on an 8x8 board (each row is one iteration):
    //      63
    //      62, 55
    //      61, 54, 47
    //      60, 53, 46, 39
    //      ...
    for y:=BOARD_SPACES-1; y>=0; y-=BOARD_SIZE {

        // check all spaces on this diagonal for queens
        queenCount := 0
        for i:=y; i<BOARD_SPACES; i+=BOARD_SIZE-1 {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // examine all backward-slanted diagonals on the top half of the board
    //      checks these array indexes on an 8x8 board:
    //      7
    //      6, 15
    //      5, 14, 23
    //      4, 13, 22, 31
    //      ...
    for y:=BOARD_SIZE-1; y<BOARD_SPACES; y+=BOARD_SIZE {

        // check all spaces on this diagonal for queens
        queenCount := 0
        for i:=y; i>=0; i-=BOARD_SIZE+1 {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // examine all backward-slanted diagonals on the bottom half of the board
    //      checks these array indexes on an 8x8 board:
    //      56
    //      57, 48
    //      58, 49, 40
    //      59, 50, 41, 32
    //      ...
    for y:=BOARD_SPACES-BOARD_SIZE; y>=0; y-=BOARD_SIZE {

        // check all spaces on this diagonal for queens
        queenCount := 0
        for i:=y; i<BOARD_SPACES; i+=BOARD_SIZE+1 {
            if string(board[i]) == "Q" {
                queenCount += 1
            }
        }

        // reduce rating if there are multiple queens
        if queenCount > 1 {
            rating -= queenCount
        }
    }

    // reduce fitness if the number of queens on the board is not BOARD_SIZE (8 on an 8x8 board)
    queenCount := strings.Count(board, "Q")
    abs := queenCount - BOARD_SIZE
    if abs < 0 {
        abs *= -1
    }
    rating -= abs * 50   // -50 points for each extra or each missing queen

    return rating
}

