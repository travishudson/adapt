package main

//
// Use the nqueens package and a genetic algorithm to solve the n queens problem, where n queens must be placed on a chess board without attacking each other.
//

import (
    "bitbucket.org/travishudson/adapt"
    "bitbucket.org/travishudson/adapt/examples/pkg/nqueens"
    "os"
    "fmt"
)

//
// Program execution begins here
//
func main() {
    var finalPopulations []adapt.Population

    // continue from a saved list of populations if the file exists
    if _,err := os.Stat("populations.gob"); err == nil {
        populations := adapt.UnserializePopulations("populations.gob")
        finalPopulations, err = adapt.SolveWithPopulations(nqueens.NQueensProblem{}, populations)
        if err != nil {
            fmt.Fprintf(os.Stderr, "Error: %v\n", err)
            os.Exit(1)
        }
    } else {
        finalPopulations, err = adapt.Solve(nqueens.NQueensProblem{})
        if err != nil {
            fmt.Fprintf(os.Stderr, "Error: %v\n", err)
            os.Exit(1)
        }
    }

    adapt.SummarizePopulations(finalPopulations)
    bestIndividual := adapt.FindMostFitIndividual(finalPopulations)
    fmt.Println(string(bestIndividual.Genome))
    //adapt.SerializePopulations(finalPopulations, "populations.gob")
}

