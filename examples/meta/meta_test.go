package main

import (
    "fmt"
    "time"
    "math"
    "reflect"
    "strings"
    "testing"
    "bitbucket.org/travishudson/adapt"
    "bitbucket.org/travishudson/adapt/examples/pkg/nqueens"
)

func TestParseDuration(t *testing.T) {
    tests := []struct {
        input string
        expected time.Duration
    }{
        {"-1blah", time.Second * time.Duration(0)},
        {"-1", time.Second * time.Duration(0)},
        {"", time.Second * time.Duration(0)},
        {"0", time.Second * time.Duration(0)},
        {"1", time.Second * time.Duration(1)},
        {"1 second", time.Second * time.Duration(1)},
        {"2seconds", time.Second * time.Duration(2)},
        {"200 seconds", time.Second * time.Duration(200)},
        {"555 second", time.Second * time.Duration(555)},
        {"1 hour", time.Hour * time.Duration(1)},
        {"2hours", time.Hour * time.Duration(2)},
        {"200 hours", time.Hour * time.Duration(200)},
        {"555 hour", time.Hour * time.Duration(555)},
        {"111blah", time.Second * time.Duration(111)},
        {"111 blah", time.Second * time.Duration(111)},
        {" 111", time.Second * time.Duration(111)},
        {" -111", time.Second * time.Duration(0)},
        {"blah2", time.Second * time.Duration(2)},
        {"blah2hours", time.Hour * time.Duration(2)},
        {"blah2HOurS", time.Hour * time.Duration(2)},
        {"200 sECOnds", time.Second * time.Duration(200)},
        {"200 minutes", time.Minute * time.Duration(200)},
        {"5minutES", time.Minute * time.Duration(5)},
    }

    for _,test := range tests {
        actual := parseDuration(test.input)
        if actual != test.expected {
            inputDisp := "'" + string(test.input) + "'"
            expectedDisp := fmt.Sprintf("'%v'", test.expected)
            t.Errorf("Input: %-14v Expected: %-10v Actual: '%v'", inputDisp, expectedDisp, actual)
        }
    }
}

//
// How the genome is divided up:
// [0,5]: PopulationSize
// [5,10]: ToKill
// [10,15]: ImmuneCount
// [15,20]: TransferRate
// [20,26]: MutationPolicy.Y
// [26,31]: MutationPolicy.Z
//
func TestGenomeToConfig(t *testing.T) {

    // Define a standard config with values that will always be the same no matter what the input is
    standardConfig := adapt.GAConfig{
        Problem: nqueens.NQueensProblem{},
        MaxGenerations: math.MaxInt32,
        MaxDuration: time.Second * time.Duration(6),
        GoroutineCount: 1}

    // Define a struct for holding the expected output values that will vary based on input
    type Output struct{
        PopulationSize int
        ToKill int
        ImmuneCount int
        TransferRate int
        MutationPolicy adapt.MutationPolicy
    }

    tests := []struct{
        commaDelimGenome string
        expected Output
    }{
        {"00001,00001,00001,00000,00000,00000", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 0, Z: 0}}}, // ok
        {"00296,00001,00001,00000,00000,00000", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 0, Z: 0}}}, // pop wraps
        {"00296,00007,00001,00000,00000,00000", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 0, Z: 0}}}, // tokill wraps
        {"00296,00007,00001,00100,00000,00000", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: 100, MutationPolicy: adapt.MutationPolicy{Y: 0, Z: 0}}}, // ok
        {"00296,00007,00001,90000,00000,00000", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: math.MaxInt16 - 1, MutationPolicy: adapt.MutationPolicy{Y: 0, Z: 0}}}, // transfer rate capped
        {"00296,00007,00001,00000,99999,00001", Output{PopulationSize: 6, ToKill: 1, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 99.999, Z: 0.001}}}, // high mutation
        {"99999,99999,00001,00000,99999,00001", Output{PopulationSize: 26, ToKill: 8, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 99.999, Z: 0.001}}}, // high tokill and pop
        {"99999,99999,00001,00000,99999,000011", Output{PopulationSize: 26, ToKill: 8, ImmuneCount: 1, TransferRate: 0, MutationPolicy: adapt.MutationPolicy{Y: 99.999, Z: 0.001}}}, // too long
        {"99999,99999,00001,00000,99999,0000", Output{}}, // too short
    }

    for _,test := range tests {
        genomeString := strings.Replace(test.commaDelimGenome, ",", "", -1)
        actual, err := genomeToConfig([]byte(genomeString))

        fmt.Println(1)
        if err != nil {
            if reflect.DeepEqual(actual, adapt.GAConfig{}) == false {
                t.Errorf("Unexpected error.")
            }
        } else {
            expected := standardConfig
            expected.PopulationSize = test.expected.PopulationSize
            expected.ToKill = test.expected.ToKill
            expected.ImmuneCount = test.expected.ImmuneCount
            expected.TransferRate = test.expected.TransferRate
            expected.MutationPolicy = test.expected.MutationPolicy

            if reflect.DeepEqual(actual, expected) == false {
                inputDisp := fmt.Sprintf("'%s'", genomeString)
                expectedDisp := fmt.Sprintf("'%v'", expected)
                t.Errorf("Input: %s Expected: %v Actual: %v", inputDisp, expectedDisp, actual)
            }
        }
    }
}

