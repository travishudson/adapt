package main

//
// Evolve what seems to be the best configuration for evolving a solution to the n queens problem,
// where n queens must be placed on a chess board without attacking each other.
//
// Copy config.json.example to something like "config.json" and pass it to the command to configure the way this example runs:
//     ./meta config.json
//

import (
    "fmt"
    "bitbucket.org/travishudson/adapt"
    "bitbucket.org/travishudson/adapt/examples/pkg/nqueens"
    "encoding/json"
    "math"
    "strings"
    "strconv"
    "sort"
    "os"
    "time"
    "regexp"
    "sync"
)

//
// The configuration file (typically "config.json") that defines how the outer (or meta) algorithm should run is parsed directly into this struct
//
type MetaConfig struct {
    MaxGenerations *int `json:"MaxGenerations"`
    MaxDuration *string `json:"MaxDuration"`
    PopulationSize *int `json:"PopulationSize"`
    ToKill *int `json:"ToKill"`
    ImmuneCount *int `json:"ImmuneCount"`
    TransferRate *int `json:"TransferRate"`
    MutationY *float32 `json:"MutationY"`
    MutationZ *float32 `json:"MutationZ"`
    GoroutineCount *int `json:"GoroutineCount"`
    SampleCount *int `json:"SampleCount"`
    SampleGoroutineCount *int `json:"SampleGoroutineCount"`
}

// MetaEvProblem implements the GAProblem interface, along with these two extra fields that the interface doesn't need.
// These extra fields are useful for this specific problem, not for any genetic algorithm in general.
type MetaEvProblem struct {
    SampleCount int           // Run an evolutionary problem, such as NQueens, this many times and return the median fitness of all those runs as the result of MeasureFitness()
    SampleGoroutineCount int  // Run this many goroutines in parallel to solve the specified evolutionary problem enough times
}

func (prob MetaEvProblem) MeasureFitness(genome []byte) int {
    return rateGenome(genome, prob.SampleCount, prob.SampleGoroutineCount)
}

func (prob MetaEvProblem) AllValidBytes() []byte {
    return []byte("0123456789")
}

func (prob MetaEvProblem) GenomeLength() int {
    return 30
}

//
// Program execution begins here
//
func main() {

    // default configuration for solving a MetaEvProblem; this can be modified with a config.json file
    // this configuration is good for a quick test, not for producing interesting results
    config := adapt.GAConfig{
              Problem: MetaEvProblem{SampleCount: 1, SampleGoroutineCount: 1},
              MaxGenerations: math.MaxInt32,
              MaxDuration: time.Second * time.Duration(1),
              PopulationSize: 4,
              ToKill: 1,
              ImmuneCount: 1,
              TransferRate: math.MaxInt32,
              MutationPolicy: adapt.MutationPolicy{Y: 0.012, Z: 3.3},
              GoroutineCount: 4}

    // read the config file passed in on the command line, as in "./meta config.json"
    if len(os.Args) > 1 {
        configFileName := os.Args[1]
        fmt.Printf("Using config file: %s\n\n", configFileName)
        fileIntoConfig(configFileName, &config)
    }

    printStartEnd(config.MaxDuration)

    // continue from a saved list of populations if the file exists
    var startingPopulations []adapt.Population
    if _,err := os.Stat("meta-populations.gob"); err == nil {
        startingPopulations = adapt.UnserializePopulations("meta-populations.gob")
    } else {

        // make new populations from scratch
        startingPopulations = append(startingPopulations, adapt.MakePopulation(config.Problem, config.PopulationSize, config.GoroutineCount))
    }
    config.FirstPopulations = startingPopulations

    fmt.Printf("\n%40s\n", "=== Config values being used ===")
    adapt.PrintConfig(config)

    // TODO: Consider having PrintConfig() be a private function because we have to print stuff that is specific to the meta problem
    if metaProb, ok := config.Problem.(MetaEvProblem); ok {
        fmt.Printf("Sample Count: %v\n", metaProb.SampleCount)
        fmt.Printf("Sample Goroutine Count: %v\n", metaProb.SampleGoroutineCount)
    }

    var finalPopulations []adapt.Population
    finalPopulations, err := adapt.SolveWithConfig(config)
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error: %v\n", err)
        os.Exit(1)
    }

    adapt.SummarizePopulations(finalPopulations)

    bestIndividual := adapt.FindMostFitIndividual(finalPopulations)
    fmt.Println("Best genome: " + string(bestIndividual.Genome))
    fmt.Println()

    fmt.Println("Best genome expressed as a GAConfig:")
    bestConfig, err := genomeToConfig(bestIndividual.Genome)
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error: %v\n", err)
        os.Exit(1)
    }

    adapt.PrintConfig(bestConfig)

    // TODO: Consider having PrintConfig() be a private function because we have to print stuff that is specific to the meta problem
    if metaProb, ok := config.Problem.(MetaEvProblem); ok {
        fmt.Printf("Sample Count: %v\n", metaProb.SampleCount)
        fmt.Printf("Sample Goroutine Count: %v\n", metaProb.SampleGoroutineCount)
    }

    adapt.SerializePopulations(finalPopulations, "meta-populations.gob")
}

//
// Return an integer that measures how fit a genome is, with higher numbers being more fit.
// A genome is fit if it encodes efficient configuration values for the running of an evolutionary algorithm.
// sampleCount -- How many times to run an evolutionary problem, such as NQueens, then return the median fitness of all runs
// sampleGoroutineCount -- How many goroutines to run in parallel to solve the evolutionary problem
//
func rateGenome(genome []byte, sampleCount int, sampleGoroutineCount int) int {

    // set aside memory to store all fitness values
    var fitnesses []int
    for i:=0; i<sampleCount; i++ {
        fitnesses = append(fitnesses, math.MinInt32)
    }

    // run several attempts in parallel, then return their median
    var wg sync.WaitGroup
    assignments := assignGoroutines(sampleCount, sampleGoroutineCount)
    for i:=0; i<sampleGoroutineCount; i++ {
        wg.Add(1)
        go func(indexesToChange []int) {
            defer wg.Done()
            for _,index := range(indexesToChange) {
                fitnesses[index] = evolveNQueens(genome)
            }
        }(assignments[i])
    }
    wg.Wait()

    return findMedian(fitnesses)
}

//
// TODO: This function was copy/pasted directly from adapt.go. Make it capital, then get it directly from the adapt package?
// Make a list, with each item being a list of slots (usually indexes of Individuals) that a particular goroutine should be assigned to work on. Each slot can be assigned to only one goroutine.
// This is used to spread out the work that multiple goroutines are doing in parallel to create new Individuals.
//
func assignGoroutines(slotsRemaining int, goroutineCount int) [][]int {

    // Make a list as long as the number of goroutines to store the results
    assignments := [][]int{}
    for i:=0; i<goroutineCount; i++ {
        assignments = append(assignments, []int{})
    }

    // Solve this problem recursively
    return assignGoroutines_(slotsRemaining, assignments)
}

func assignGoroutines_(slotsRemaining int, assignments [][]int) [][]int {

    // No goroutines to assign slots to
    if len(assignments) == 0 {
        return assignments
    }

    // No slots to assign goroutines to
    if slotsRemaining <= 0 {
        return assignments
    }

    // Decide what slots the next goroutine should be assigned to
    slotCount := slotsRemaining / len(assignments)
    for i:=0; i<slotCount; i++ {
        slotsRemaining--
        assignments[0] = append(assignments[0], slotsRemaining)
    }
    return append([][]int{assignments[0]}, assignGoroutines_(slotsRemaining, assignments[1:])...)
}

//
// Use an evolutionary algorithm to solve nqueens, configured with the specified genome.
// Return the best fitness among all populations that resulted from it.
//
func evolveNQueens(genome []byte) int {
    config, err := genomeToConfig(genome)
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error: %v\n", err)
        os.Exit(1)
    }

    finalPopulations, err := adapt.SolveWithConfig(config)
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error: %v\n", err)
        return math.MinInt32 // return an extremely negative fitness if the genome causes an error
    }

    // find highest fitness value
    var fitnesses []int
    for _,pop := range finalPopulations {
        fitnesses = append(fitnesses, pop.Individuals[0].Fitness)
    }
    sort.Sort(sort.Reverse(sort.IntSlice(fitnesses)))
    bestFitness := fitnesses[0]

    // print this solution for nqueens if it's especially good
    if bestFitness > -7 {
        bestGenome := ""
        for _,pop := range finalPopulations {
            if pop.Individuals[0].Fitness == bestFitness {
                bestGenome = string(pop.Individuals[0].Genome)
            }
        }
        fmt.Printf("nqueens solution fitness and genome: %d\n%s\n\n", bestFitness, bestGenome)
    }

    // the overall fitness for this genome is the best fitness produced by using it
    return bestFitness
}

//
// Find median value of ints, rounded to the nearest int
//
func findMedian(nums []int) int {
    sort.Ints(nums)

    length := len(nums)
    if length == 0 {
        return 0
    } else if length == 1 {
        return nums[0]
    }

    // average the middle values if the length of the list is even
    if length % 2 == 0 {
        return roundToInt(float64(nums[(length / 2) - 1] + nums[length / 2]) / 2)
    } else {
        return roundToInt(float64(nums[length / 2]))
    }
}

//
// Round a float64 to the nearest integer value
//
func roundToInt(num float64) int {
    if math.Ceil(num) - num > 0.5 {
        return int(math.Floor(num))
    } else {
        return int(math.Ceil(num))
    }
}

//
// Convert a list of bytes into configuration settings for an evolutionary algorithm (populationSize, toKill, etc.)
//
// How the genome is divided up:
// [0,5]: PopulationSize
// [5,10]: ToKill
// [10,15]: ImmuneCount
// [15,20]: TransferRate
// [20,26]: MutationPolicy.Y
// [26,31]: MutationPolicy.Z
//
func genomeToConfig(genome []byte) (adapt.GAConfig, error) {

    if len(genome) < 30 {
        return adapt.GAConfig{}, fmt.Errorf("Genome is too short. Length is %d. Expected %d", len(genome), 30)
    }

    result := adapt.GAConfig{
              Problem: nqueens.NQueensProblem{},
              MaxGenerations: math.MaxInt32,
              MaxDuration: time.Second * time.Duration(6),
              PopulationSize: intFromBytes(genome, 0, 5),
              ToKill: intFromBytes(genome, 5, 10),
              ImmuneCount: intFromBytes(genome, 10, 15),
              TransferRate: intFromBytes(genome, 15, 20),
              MutationPolicy: adapt.MutationPolicy{Y: floatFromBytes(genome,20,25), Z: floatFromBytes(genome,25,30)},
              GoroutineCount: 1}

    // make sure that the result is a valid configuration
    if result.TransferRate == 0 { result.TransferRate = 1 }
    result.PopulationSize = (result.PopulationSize % 295) + 5
    result.ToKill = (result.ToKill % (result.PopulationSize - 4)) + 1
    result.ImmuneCount = result.ImmuneCount % (result.PopulationSize - result.ToKill)

    return result, nil
}

//
// Read a configuration file such as "config.json" and return a corresponding MetaConfig object
//
func fileToConfig(fileName string) MetaConfig {
    file, err := os.Open(fileName)
    if err != nil { panic(err) }
    decoder := json.NewDecoder(file)
    config := MetaConfig{}
    err = decoder.Decode(&config)
    if err != nil { panic(err) }
    return config
}

//
// Read a configuration file such as "config.json" into an existing config object
// If any field in the file is not specified in the json file, it will be not be altered
//
func fileIntoConfig(fileName string, config *adapt.GAConfig) {
    file := fileToConfig(fileName)

    if file.MaxGenerations != nil { config.MaxGenerations = *file.MaxGenerations }
    if file.MaxDuration != nil { config.MaxDuration = parseDuration(*file.MaxDuration) }
    if file.PopulationSize != nil { config.PopulationSize = *file.PopulationSize }
    if file.ToKill != nil { config.ToKill = *file.ToKill }
    if file.ImmuneCount != nil { config.ImmuneCount = *file.ImmuneCount }
    if file.TransferRate != nil { config.TransferRate = *file.TransferRate }
    if file.MutationY != nil && file.MutationZ != nil { config.MutationPolicy = adapt.MutationPolicy{Y: *file.MutationY, Z: *file.MutationZ} }
    if file.GoroutineCount != nil { config.GoroutineCount = *file.GoroutineCount }
    if file.SampleCount != nil && file.SampleGoroutineCount != nil { config.Problem = MetaEvProblem{SampleCount: *file.SampleCount, SampleGoroutineCount: *file.SampleGoroutineCount} }
}

//
// Make a starting population of individuals from the same genome
//
func makeIsogeneticPopulation(problem adapt.GAProblem, size int, genome []byte) adapt.Population {

    // fitness stays the same if genome is the same, so remember fitness and apply it to all
    fitness := problem.MeasureFitness(genome)

    var population adapt.Population
    for i:=0; i<size; i++ {
        population.Individuals = append(population.Individuals, adapt.Individual{Fitness: fitness, Genome: genome, AncestorCount: 0})
    }

    population.GenerationCount = 0  // number of generations completed by this population

    return population
}

//
// Convert a series of bytes to an int
//
func intFromBytes(bytes []byte, startIndex int, endIndex int) int {
    result, _ := strconv.Atoi(string(bytes)[startIndex:endIndex])

    // make sure that the result cannot be greater than 32,766, so that the result can be stored as an int16
    if result >= math.MaxInt16 {
        return math.MaxInt16 - 1
    }

    return result
}

//
// Convert a series of bytes to a five digit float that has three decimal places
//
func floatFromBytes(bytes []byte, startIndex int, endIndex int) float32 {
    floatString := string(bytes)[startIndex:startIndex+2] + "." + string(bytes)[startIndex+2:endIndex]
    result, _ := strconv.ParseFloat(floatString, 32)
    return float32(result)
}

//
// Take an input string such as:
// "1 second", or "2 hours"
// and return a duration. Negative duration is considered 0.
//
func parseDuration(s string) time.Duration {

    // Find the first occurrence of a number
    var firstNumber int
    numFinder := regexp.MustCompile("-?\\d+")
    numBounds := numFinder.FindStringIndex(s)
    if numBounds == nil {
        return time.Duration(0) // There is no number, so return nothing
    } else {
        firstNumber, _ = strconv.Atoi(s[numBounds[0]:numBounds[1]])
        if firstNumber < 0 {
            firstNumber = 0
        }
    }

    // Find the first occurrence of "second" or "minute" after the number
    afterNum := strings.ToLower(s[numBounds[1]:])
    if strings.Index(afterNum, "hour") != -1 {
        return time.Hour * time.Duration(firstNumber)
    } else if strings.Index(afterNum, "minute") != -1 {
        return time.Minute * time.Duration(firstNumber)
    } else {
        return time.Second * time.Duration(firstNumber)
    }
}

//
// Print the expected start and end time, based on duration
//
func printStartEnd(duration time.Duration) {
    now := time.Now()
    timeFormat := "2006-01-02 15:04:05"
    fmt.Printf("Start time:%44s\n", now.Format(timeFormat))
    endTime := now.Add(duration)
    fmt.Printf("End time if MaxDuration is reached: %s\n", endTime.Format(timeFormat))
}

