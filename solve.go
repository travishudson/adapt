package adapt

import (
    "math/rand"
    "runtime"
    "math"
    "sort"
    "time"
)

// Reasonable configuration settings for a genetic algorithm
var defaultConfig GAConfig = GAConfig {
    MaxGenerations: math.MaxInt32,
    MaxDuration: time.Second * time.Duration(10),
    PopulationSize: 300,
    ToKill: 150,          // how many members of the population to kill each generation
    ImmuneCount: 5,       // how many of the most fit are immune from being killed each generation
    TransferRate: 5,      // after this many generations have passed, and the most fit individual in a population still
                          // isn't any more fit, pull and push to the shared channel to get outside help from other populations
    MutationPolicy: MutationPolicy{Y: 0.0015, Z: 30.0},
    GoroutineCount: runtime.NumCPU() - 1}

//
// Wrapper for SolveWithConfig() that uses sensible defaults
//
func Solve(problem GAProblem) ([]Population, error) {
    defaultConfig.Problem = problem
    return SolveWithConfig(defaultConfig)
}

//
// Wrapper for SolveWithConfig() that uses sensible defaults and a pre-defined set of starting populations
//
func SolveWithPopulations(problem GAProblem, populations []Population) ([]Population, error) {
    defaultConfig.Problem = problem
    defaultConfig.FirstPopulations = populations
    return SolveWithConfig(defaultConfig)
}

//
// Using custom configuration values, solve a problem using an evolutionary algorithm strategy to do so.
// Return the final populations after a stopping point has been reached.
//
func SolveWithConfig(config GAConfig) ([]Population, error) {

    // Error out if the configuration settings are not valid
    err := validateGAConfig(config)
    if err != nil {
        return []Population{}, err
    }

    // Make a random number generator
    generator := rand.New(rand.NewSource(time.Now().UTC().UnixNano()))

    // Start the timer that doesn't let this function run past the maximum allowed time
    haltTimer := time.NewTimer(config.MaxDuration)

    // Add an entry to FirstPopulations if the list is empty (having zero populations to work with is not valid)
    if len(config.FirstPopulations) == 0 {
        config.FirstPopulations = append(config.FirstPopulations, MakePopulation(config.Problem, config.PopulationSize, config.GoroutineCount))
    }

    // Make a list of populations to operate on that is the same size as the list of FirstPopulations
    // Where FirstPopulations contains a population with an empty list of Individuals, add Individuals
    var populations []Population
    for i:=0; i<len(config.FirstPopulations); i++ {
        if len(config.FirstPopulations[i].Individuals) == 0 {
            populations = append(populations, MakePopulation(config.Problem, config.PopulationSize, config.GoroutineCount))
        } else {
            populations = append(populations, config.FirstPopulations[i])
        }
    }

    // Cycle through every population, considering each one separately, one at a time
    halt := false
    for halt == false {
        for p:=0; p<len(populations); p++ {

            // Sort by fitness in reverse order, with most fit first
            sort.Slice(populations[p].Individuals, func (i, j int) bool { return populations[p].Individuals[i].Fitness > populations[p].Individuals[j].Fitness })

            // If there is a stagnation problem, transfer the most fit individual from another population into the current one
            if populations[p].FitnessStagnation >= config.TransferRate {
                mostFit := FindMostFitIndividual(populations)
                if mostFit.Fitness > populations[p].HighestFitness {
                    populations[p].Individuals[len(populations[p].Individuals)-1] = mostFit
                }
            }

            kill(&populations[p], config.ToKill, config.ImmuneCount, generator)

            mutationRate := calculateMutationRate(config.MutationPolicy, populations[p].FitnessStagnation)
            repopulate(config.Problem, &populations[p], config.ToKill, mutationRate, config.GoroutineCount)

            // Update highest fitness
            newHighest := findHighestFitness(populations[p])
            if newHighest > populations[p].HighestFitness {
                populations[p].HighestFitness = newHighest
                populations[p].FitnessStagnation = 0
            } else {
                populations[p].FitnessStagnation++
            }

            // Halt if the max number of generations has been reached
            populations[p].GenerationCount += 1
            if populations[p].GenerationCount >= config.MaxGenerations {
                halt = true
                break
            }

            // Halt if time has exceeded config.MaxDuration
            select {
            case <-haltTimer.C:
                halt = true
                break
            default:
            }
        }
    }

    return populations, nil
}

