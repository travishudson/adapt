# short-term
Write unit tests
Resolve every issue brought up by "go tool vet -all ."
Allow for a way to guarantee that a meaningful mutation happened, one that impacts the phenotype (do test runs to see how much this speeds things up)
Add a way to modify a .gob by adding/removing pops and seeing basic stats
Allow starting population genomes in config file
Keep track of how long a generation gob has been run in total, and how each run went, storing that data in the gob file itself

# long-term
Allow processor count and goroutine count to differ
Make chromosome-based version
Make a version where the user enters multiple MeasureFitness() functions, then the alorithm randomly applies them individually to keep continuous variation high
Make a linked list-based version where related populations naturally form because proximity is taken into account when selecting a mate
Use the Amazon compute cloud
Re-implement in Clojure or Haskell for functional clarity

