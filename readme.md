Readme for Adapt Go Package
===========================

## This go package's purpose
This is an evolutionary algorithm designed to solve problems in a way that mimics natural evolution.
It is best suited for situations in which the desired solution just has to be good, not perfect, and there are too many possible solutions to check individually.
Short examples are located in examples/

## How to install this package
Install go from golang.org

Set up GOPATH to be anywhere convenient:
    export GOPATH=$HOME/path/to/packages

Download this package:
    go get bitbucket.org/travishudson/adapt

It is now ready for use in any go program with:
    import "bitbucket.org/travishudson/adapt"
    ...
    ...
    ...
    adapt.Solve(...)

Build one of the examples, then run it:
    cd ~/Desktop
    go build bitbucket.org/travishudson/adapt/examples/eightqueen
    ./eightqueen

## Ways of using this package
Start a documentation server on the local machine, which allows a browser to visit [](http://localhost:6060/pkg/bitbucket.org/travishudson/adapt/)
    godoc -http=":6060" -notes=".*"

Print source code only for the few exported functions (such as adapt.Solve(), adapt.Continue(), ...)
    godoc -src bitbucket.org/travishudson/adapt

## List of contributors
Travis Hudson [original creator of this package]

